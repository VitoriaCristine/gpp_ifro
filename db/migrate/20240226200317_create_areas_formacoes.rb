class CreateAreasFormacoes < ActiveRecord::Migration[7.0]
  def change
    create_table :areas_formacoes do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
