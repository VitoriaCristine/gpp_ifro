class CreateDisciplinas < ActiveRecord::Migration[7.0]
  def change
    create_table :disciplinas do |t|
      t.string :descricao
      t.references :area_formacao, null: false, foreign_key: true
      t.integer :horas_semanais

      t.timestamps
    end
  end
end
