class AddDeletedAtToAllModels < ActiveRecord::Migration[7.0]
  def up
    add_column :areas_formacoes, :deleted_at, :datetime
    add_column :disciplinas, :deleted_at, :datetime
    add_column :niveis, :deleted_at, :datetime
    add_column :preferencias, :deleted_at, :datetime
    add_column :turmas, :deleted_at, :datetime
    add_column :turnos, :deleted_at, :datetime
  end

  def down
    remove_column :areas_formacoes, :deleted_at, :datetime
    remove_column :disciplinas, :deleted_at, :datetime
    remove_column :niveis, :deleted_at, :datetime
    remove_column :preferencias, :deleted_at, :datetime
    remove_column :turmas, :deleted_at, :datetime
    remove_column :turnos, :deleted_at, :datetime
  end
end
