class CreateTurmas < ActiveRecord::Migration[7.0]
  def change
    create_table :turmas do |t|
      t.string :descricao
      t.references :nivel, null: false, foreign_key: true
      t.references :turno, null: false, foreign_key: true

      t.timestamps
    end
  end
end
