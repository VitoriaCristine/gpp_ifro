class AddCamposToUser < ActiveRecord::Migration[7.0]
  def up
    add_column :users, :nome,       :string
    add_column :users, :cpf,        :string
    add_column :users, :admin,      :boolean
    add_column :users, :created_by, :string
    add_column :users, :updated_by, :string
    add_column :users, :deleted_at, :datetime
  end

  def down
    remove_column :users, :nome,       :string
    remove_column :users, :cpf,        :string
    remove_column :users, :admin,      :boolean
    remove_column :users, :created_by, :string
    remove_column :users, :updated_by, :string
    remove_column :users, :deleted_at, :datetime    
  end
end
