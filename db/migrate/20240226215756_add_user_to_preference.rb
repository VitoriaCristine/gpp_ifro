class AddUserToPreference < ActiveRecord::Migration[7.0]
  def up
    add_reference :preferencias, :user, foreing_key: true
  end

  def down
    remove_reference :preferencias, :user, foreing_key: true
  end
end
