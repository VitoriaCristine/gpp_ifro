class RemoveUsersFromPreferencia < ActiveRecord::Migration[7.0]
  def up
    remove_reference :users, :preferencia, foreing_key: true
  end

  def down
    add_reference :users, :preferencia, foreing_key: true
  end
end
