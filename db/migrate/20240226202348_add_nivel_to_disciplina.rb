class AddNivelToDisciplina < ActiveRecord::Migration[7.0]
  def up
    add_reference :disciplinas, :nivel, foreing_key: true
  end

  def down
    remove_reference :disciplinas, :nivel, foreing_key: true
  end
end
