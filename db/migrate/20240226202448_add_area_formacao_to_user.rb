class AddAreaFormacaoToUser < ActiveRecord::Migration[7.0]
  def up
    add_reference :users, :area_formacao, foreing_key: true
    add_reference :users, :preferencia, foreing_key: true
  end

  def down
    remove_reference :users, :area_formacao, foreing_key: true
    remove_reference :users, :preferencia, foreing_key: true
  end
end
