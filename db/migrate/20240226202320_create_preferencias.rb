class CreatePreferencias < ActiveRecord::Migration[7.0]
  def change
    create_table :preferencias do |t|
      t.references :turno, null: false, foreign_key: true
      t.references :disciplina, null: false, foreign_key: true

      t.timestamps
    end
  end
end
