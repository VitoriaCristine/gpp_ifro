# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_02_26_215756) do
  create_table "areas_formacoes", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "disciplinas", force: :cascade do |t|
    t.string "descricao"
    t.integer "area_formacao_id", null: false
    t.integer "horas_semanais"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "nivel_id"
    t.datetime "deleted_at"
    t.index ["area_formacao_id"], name: "index_disciplinas_on_area_formacao_id"
    t.index ["nivel_id"], name: "index_disciplinas_on_nivel_id"
  end

  create_table "niveis", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "preferencias", force: :cascade do |t|
    t.integer "turno_id", null: false
    t.integer "disciplina_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "user_id"
    t.index ["disciplina_id"], name: "index_preferencias_on_disciplina_id"
    t.index ["turno_id"], name: "index_preferencias_on_turno_id"
    t.index ["user_id"], name: "index_preferencias_on_user_id"
  end

  create_table "turmas", force: :cascade do |t|
    t.string "descricao"
    t.integer "nivel_id", null: false
    t.integer "turno_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["nivel_id"], name: "index_turmas_on_nivel_id"
    t.index ["turno_id"], name: "index_turmas_on_turno_id"
  end

  create_table "turnos", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nome"
    t.string "cpf"
    t.boolean "admin"
    t.string "created_by"
    t.string "updated_by"
    t.datetime "deleted_at"
    t.integer "area_formacao_id"
    t.index ["area_formacao_id"], name: "index_users_on_area_formacao_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "disciplinas", "areas_formacoes"
  add_foreign_key "preferencias", "disciplinas"
  add_foreign_key "preferencias", "turnos"
  add_foreign_key "turmas", "niveis"
  add_foreign_key "turmas", "turnos"
end
