ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular "turno", "turnos"
  inflect.irregular "area_formacao", "areas_formacoes"
  inflect.irregular "disciplina", "disciplinas"
  inflect.irregular "nivel", "niveis"
  inflect.irregular "turma", "turmas"
  inflect.irregular "preferencia", "preferencias"
end

