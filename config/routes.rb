Rails.application.routes.draw do  
  resources :preferencias
  resources :preferencia
  resources :turmas
  resources :niveis
  resources :disciplinas
  resources :areas_formacoes
  resources :turnos
  devise_for :users, skip: [:registrations], path: "auth"
  resources :users, path: "usuarios"
  root "home#index"
end
