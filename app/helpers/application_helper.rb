module ApplicationHelper
  include Pagy::Frontend
  
  def btn_submit(form)
    text = form.object.new_record? ? "Incluir" : "Atualizar"
    icon_class = "ph-paper-plane-tilt ms-1"
    
    button_tag(type: "submit", class: "btn btn-success") do
      safe_join([text, content_tag(:i, "", class: icon_class)])
    end
  end
  
  def formatar_boolean_xlsx(value)
    value ? 'Sim' : 'Não'
  end
  
  def formatar_data(data)
    data.strftime("%d/%m/%Y") if data
  end
  
  def formatar_percentual(value)
    value.present? ? number_to_percentage(value, precision: 0) : "0%"
  end
  
  def formatar_real(value)
    value.present? ? number_to_currency(value, unit: "", separator: ",", delimiter: ".") : "0,00"
  end
  
  def formatar_cpf(cpf)
    cpf.gsub(/(\d{3})(\d{3})(\d{3})(\d{2})/, '\1.\2.\3-\4') if cpf
  end
  
  def formatar_boolean(value)
    if value
      '<span class="badge bg-success bg-opacity-20 text-success ms-2">Sim</span>'.html_safe
    else
      '<span class="badge bg-danger bg-opacity-20 text-danger ms-2">Não</span>'.html_safe
    end
  end
    
  def formatar_processo_sei(processo_sei)
    processo_sei.gsub(/(\d{4})(\d{6})(\d{4})(\d{2})/, '\1.\2/\3-\4') if processo_sei
  end  
end
