class TurmasController < ApplicationController
  before_action :set_turma, only: %i[ edit update destroy ]

  def index
    @q = Turma.ransack(params[:q])
    @pagy, @turmas = pagy(@q.result)
  end

  def new
    @turma = Turma.new
  end

  def edit
  end

  def create
    @turma = Turma.new(turma_params)

    if @turma.save
      redirect_to turmas_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @turma.update(turma_params)
      redirect_to turmas_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @turma.soft_delete
      redirect_to turmas_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to turmas_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_turma
    @turma = Turma.find(params[:id])
  end

  def turma_params
    params.require(:turma).permit(Turma.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
