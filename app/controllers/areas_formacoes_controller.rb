class AreasFormacoesController < ApplicationController
  before_action :set_area_formacao, only: %i[ edit update destroy ]

  def index
    @q = AreaFormacao.ransack(params[:q])
    @pagy, @areas_formacoes = pagy(@q.result)
  end

  def new
    @area_formacao = AreaFormacao.new
  end

  def edit
  end

  def create
    @area_formacao = AreaFormacao.new(area_formacao_params)

    if @area_formacao.save
      redirect_to areas_formacoes_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @area_formacao.update(area_formacao_params)
      redirect_to areas_formacoes_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @area_formacao.soft_delete
      redirect_to areas_formacoes_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to areas_formacoes_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_area_formacao
    @area_formacao = AreaFormacao.find(params[:id])
  end

  def area_formacao_params
    params.require(:area_formacao).permit(AreaFormacao.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
