class NiveisController < ApplicationController
  before_action :set_nivel, only: %i[ edit update destroy ]

  def index
    @q = Nivel.ransack(params[:q])
    @pagy, @niveis = pagy(@q.result)
  end

  def new
    @nivel = Nivel.new
  end

  def edit
  end

  def create
    @nivel = Nivel.new(nivel_params)

    if @nivel.save
      redirect_to niveis_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @nivel.update(nivel_params)
      redirect_to niveis_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @nivel.soft_delete
      redirect_to niveis_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to niveis_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_nivel
    @nivel = Nivel.find(params[:id])
  end

  def nivel_params
    params.require(:nivel).permit(Nivel.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
