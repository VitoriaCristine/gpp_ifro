class DisciplinasController < ApplicationController
  before_action :set_disciplina, only: %i[ edit update destroy ]

  def index
    @q = Disciplina.includes(:nivel, :area_formacao).ransack(params[:q])
    @pagy, @disciplinas = pagy(@q.result)
  end

  def new
    @disciplina = Disciplina.new
  end

  def edit
  end

  def create
    @disciplina = Disciplina.new(disciplina_params)

    if @disciplina.save
      redirect_to disciplinas_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @disciplina.update(disciplina_params)
      redirect_to disciplinas_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @disciplina.soft_delete
      redirect_to disciplinas_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to disciplinas_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_disciplina
    @disciplina = Disciplina.find(params[:id])
  end

  def disciplina_params
    params.require(:disciplina).permit(Disciplina.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
