class TurnosController < ApplicationController
  before_action :set_turno, only: %i[ edit update destroy ]

  def index
    @q = Turno.ransack(params[:q])
    @pagy, @turnos = pagy(@q.result)
  end

  def new
    @turno = Turno.new
  end

  def edit
  end

  def create
    @turno = Turno.new(turno_params)

    if @turno.save
      redirect_to turnos_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @turno.update(turno_params)
      redirect_to turnos_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @turno.soft_delete
      redirect_to turnos_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to turnos_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_turno
    @turno = Turno.find(params[:id])
  end

  def turno_params
    params.require(:turno).permit(Turno.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
