class PreferenciasController < ApplicationController
  before_action :set_preferencia, only: %i[ edit update destroy ]

  def index
    @q = Preferencia.ransack(params[:q])
    @pagy, @preferencias = pagy(@q.result)
  end

  def new
    @preferencia = Preferencia.new
  end

  def edit
  end

  def create
    @preferencia = Preferencia.new(preferencia_params.merge(user: current_user))

    if @preferencia.save
      redirect_to preferencias_path, notice: "Registro cadastrado com sucesso!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @preferencia.update(preferencia_params)
      redirect_to preferencias_path, notice: "Registro atualizado com sucesso!", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @preferencia.soft_delete
      redirect_to preferencias_url, notice: "Registro foi marcado como excluído."
    else
      redirect_to preferencias_url, alert: "Ainda existem dependentes desse registro."
    end   
  end

  private

  def set_preferencia
    @preferencia = Preferencia.find(params[:id])
  end

  def preferencia_params
    params.require(:preferencia).permit(Preferencia.column_names.reject { |col| ['deleted_at', 'created_by', 'updated_by'].include?(col) }.map(&:to_sym))    
  end
end
