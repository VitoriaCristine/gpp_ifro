class AreaFormacao < ApplicationRecord
  has_many :disciplinas
  has_many :users

  validates :descricao, presence: true, uniqueness: true

  def descricao=(descricao)
    super descricao.upcase.strip
  end

  def to_s
    descricao
  end
end
