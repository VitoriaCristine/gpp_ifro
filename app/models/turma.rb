class Turma < ApplicationRecord
  belongs_to :nivel
  belongs_to :turno

  validates :descricao, presence: true, uniqueness: true

  def descricao=(descricao)
    super descricao = descricao.upcase
  end
end
