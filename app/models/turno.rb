class Turno < ApplicationRecord
  has_many :turmas
  has_many :preferencias

  validates :descricao, presence: true, uniqueness: true

  def descricao=(descricao)
    super descricao.upcase.strip
  end

  def to_s
    descricao
  end
end
