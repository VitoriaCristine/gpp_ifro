class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, authentication_keys: [:cpf]
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, authentication_keys: [:cpf]
  
  
  validates :cpf, presence: true, uniqueness: true
  validates :nome, presence: true, uniqueness: true
  validates :email, presence: true
  validate :custom_validates
  
  has_many :preferencias
  
  def nome=(nome)
    super nome = nome.upcase
  end

  def cpf=(cpf)
    super cpf = cpf.gsub(/[.-]/, "")
  end
  
  def custom_validates
    # Validação de CPF
    unless CPF.valid?(cpf)
      errors.add(:cpf, "não é válido")
    end          
  end
end
  