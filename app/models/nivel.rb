class Nivel < ApplicationRecord
  has_many :disciplinas
  has_many :turmas

  validates :descricao, presence: true, uniqueness: true

  def to_s
    descricao
  end
end
