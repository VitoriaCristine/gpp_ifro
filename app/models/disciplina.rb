class Disciplina < ApplicationRecord
  belongs_to :area_formacao
  belongs_to :nivel

  validates :descricao, presence: true, uniqueness: true
  validates :horas_semanais, presence: true

  def descricao=(descricao)
    super descricao.upcase.strip
  end

  def to_s
    descricao
  end
end
