class Preferencia < ApplicationRecord
  belongs_to :turno
  belongs_to :disciplina
  belongs_to :user
end
